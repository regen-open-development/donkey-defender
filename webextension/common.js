/*
Regen Open Development 2018
Licensed under the Mozilla Public License Version 2.0.
Refer to README.md and LICENSE.md for details.
*/

/*
Browser independence layer / wrapper objects.
Chrome functionality is adapted to make use of promises instead of callbacks.
*/
var common = {};
common.storage = {};
common.storage.local = {};
common.webNavigation = {};
common.tabs = {};
common.notifications = {};

common.storage.local.get = (() => {
  if(typeof browser !== "undefined") {
    return browser.storage.local.get;
  }
  else if (typeof chrome !== "undefined") {
    return (key) => new Promise( (resolve, reject) => {
        chrome.storage.sync.get(key, resolve);
    });
  }
  return function(){};
})();

common.storage.local.set = (() => {
  if(typeof browser !== "undefined") {
    return browser.storage.local.set;
  }
  else if (typeof chrome !== "undefined") {
    return (dict) => new Promise( (resolve, reject) => {
        chrome.storage.sync.set(dict, resolve)
      });
  }
  return null;
})();

common.webNavigation.onBeforeNavigate = (() => {
  if(typeof browser !== "undefined") {
    return browser.webNavigation.onBeforeNavigate;
  }
  else if (typeof chrome !== "undefined") {
    return chrome.webNavigation.onBeforeNavigate;
  }
  return null;
})();

common.tabs.update = (() => {
  if(typeof browser !== "undefined") {
    return function(tabId, updateProperties)
    {
      browser.runtime.getPlatformInfo().then( (info) => {
        if(info.os == "android") {
          delete updateProperties.loadReplace;
          browser.tabs.update(tabId, updateProperties);
        } else {
          browser.tabs.update(tabId, updateProperties);
        }
      });
    };
  }
  else if (typeof chrome !== "undefined") {
    return function(tabId, updateProperties){
      delete updateProperties.loadReplace;
      chrome.tabs.update(tabId, updateProperties);
    };
  }
  return null;
})();

common.tabs.getCurrent = (() => {
  if(typeof browser !== "undefined") {
    return browser.tabs.getCurrent;
  }
  else if (typeof chrome !=="undefined") {
    return () => {
      return new Promise( (resolve, reject) => {
        chrome.tabs.getCurrent(resolve);
      });
    };
  }
  return null;
})();

common.notifications.create = (() => {
  if(typeof browser !== "undefined") {
    return browser.notifications.create;
  }
  else if (typeof chrome !== "undefined") {
    return (key, properties) => new Promise( (resolve, reject)=>{
      chrome.notifications.create(key, properties, resolve);
    });
  }
  return null;
})();

common.isMobile = (() => { return new Promise(function( resolve, reject ){
    if(typeof browser !== "undefined") {
      browser.runtime.getPlatformInfo().then( (info) => {
        if (info.os === "android") {
          resolve();
        } else {
          reject(Err("Donkey Defender: Platform is not Android!"));
        }
      });
    }
    else if (typeof chrome !== "undefined") {
      chrome.runtime.getPlatformInfo().then( (info) => {
        if (info.os === "android") {
          resolve();
        } else {
          reject(Err("Donkey Defender: Platform is not Android!"));
        }
      });
    }
  });
});

common.isDesktop = (() => { return new Promise(function( resolve, reject ){
    if(typeof browser !== "undefined") {
      browser.runtime.getPlatformInfo().then( (info) => {
        if (info.os !== "android") {
          resolve();
        } else {
          reject(Err("Donkey Defender: Platform is not Desktop!"));
        }
      });
    }
    else if (typeof chrome !== "undefined") {
      chrome.runtime.getPlatformInfo( (info) => {
        if (info.os !== "android") {
          resolve();
        } else {
          reject(Err("Donkey Defender: Platform is not Desktop!"));
        }
      });
    }
  });
});
