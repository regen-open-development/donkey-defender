/*
Regen Open Development 2018
Licensed under the Mozilla Public License Version 2.0.
Refer to README.md and LICENSE.md for details.
*/

var difference = cut_decimal(parseFloat(getVariable("difference")) * 100, 2);
var protectedDomain = getVariable("protectedDomain");
var blockedDomain = getVariable("blockedDomain");
var attemptedConnection = getVariable("attemptedConnection");

function initialize() {
  place_variable_in_class("target", blockedDomain);
  place_variable_in_class("protected", protectedDomain);
  place_variable_in_class("percent", difference);
  document.getElementById("whitelistButton").addEventListener("click", tryAddToWhitelist );
}

/* Utility function for quickly getting variable from
 * within a url string. */
function getVariable(variable) {
  let parsedUrl = new URL(window.location.href);
  return parsedUrl.searchParams.get(variable);
}

/* Takes a value and removes unnecessary decimals.
 * e.g.: cut_decimal(2.53426123, 3) = 2.534 */
function cut_decimal(float, decimal_offset) {
  let scale = Math.pow(10, decimal_offset);
  return Math.round( float * scale ) / scale;
}

function place_variable_in_class( className, valueToFill ) {
  let classInstances = document.getElementsByClassName(className);

  for(let i=0; i<classInstances.length; i++){
    classInstances[i].textContent = valueToFill;
  }
}

function tryAddToWhitelist() {
  function addToWhitelist(domain, whitelist) {
    whitelist = new Set(whitelist);
    whitelist.add(domain);

    common.storage.local.set( {
      whitelistedDomains: [...whitelist]
    });

    common.notifications.create("whitelistNotification", {
      type:"basic",
      message: domain + " added to whitelist.",
      title: "Donkey Defender",
      iconUrl: "/resources/icon128.png"
    });

    common.tabs.getCurrent().then( (tab)=>{
        common.tabs.update(tab.id,
          {
          loadReplace: true,
          url:attemptedConnection
          }
        );
      }
    );
  }

  common.storage.local.get("whitelistedDomains").then( (option) => {
    addToWhitelist(blockedDomain, option.whitelistedDomains)
  }, (err) => {
    addToWhitelist(blockedDomain, new Set());
  } );
}

document.addEventListener("DOMContentLoaded", initialize);
