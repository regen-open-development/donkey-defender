/*
Regen Open Development 2018
Licensed under the Mozilla Public License Version 2.0.
Refer to README.md and LICENSE.md for details.
*/

/*
WebExtension options page logic.
*/

function saveOptions(evnt) {
  evnt.preventDefault();
  // Remove spaces and split at semicolons or commas, creating a set.
  let protectedDomains = new Set(document.querySelector("#protectedDomains").value.replace(/\s+/g,'')
                                                                                   .replace(/,/g, ";")
                                                                                   .toLowerCase()
                                                                                   .split(';'));
  protectedDomains = [...protectedDomains];

  let whitelistedDomains = new Set(document.querySelector("#whitelistedDomains").value.replace(/\s+/g, '')
                                                                                      .replace(/,/g, ";")
                                                                                      .toLowerCase()
                                                                                      .split(';'));
  whitelistedDomains = [...whitelistedDomains];

  let threshold = document.querySelector("#security_level").value / 100;

  common.storage.local.set({
    protectedDomains: protectedDomains,
    whitelistedDomains: whitelistedDomains,
    threshold: threshold
  }).then((_) => {
    location.reload();
  });
}

function loadOptions() {
  function onError(error) {
    console.log("Error: ${error}");
  }

  common.storage.local.get("protectedDomains").then(
  (value) => {
    if(Array.isArray(value.protectedDomains)) {
      document.querySelector("#protectedDomains").value = value.protectedDomains.join(";");
    }
  }, (e) => { onError(e); }  );

  common.storage.local.get("whitelistedDomains").then(
  (value) => {
    if(Array.isArray(value.whitelistedDomains)) {
      document.querySelector("#whitelistedDomains").value = value.whitelistedDomains.join(";");
    }
  }, (e) => { onError(e); } );

  common.storage.local.get("threshold").then(
  (value) => {
    document.querySelector("#security_level").value = value.threshold * 100;
    onThresholdChange();
  }, (e) => { onError(e); });

}

function onThresholdChange() {
  let slider = document.querySelector("#security_level");
  let string = document.querySelector("#security_level_string");
  let level_percent = document.querySelector("#security_level_percent");

  let percentage = slider.value + "% ";
  level_percent.innerText = percentage;
  if (slider.value >= 30.0) {
    string.innerText = "(HARSH)";
    string.style.color = "#ef372d"
  } else if (slider.value >= 15.0) {
    string.innerText = "(STRICT)";
    string.style.color = "#ef8436";
  } else if (slider.value > 0.0) {
    string.innerText = "(LENIENT)";
    string.style.color = "#efd53f";
  } else if (slider.value == 0) {
    string.innerText = "(OFF)";
    string.style.color = "#000000";
  }
}

function changeButton() {
  let save = document.querySelector("#save");
  save.classList.add("button-primary");
}

document.addEventListener("DOMContentLoaded", loadOptions);
document.querySelector("form").addEventListener("submit", saveOptions);
document.querySelector("form").addEventListener("change", changeButton);
document.querySelector("#security_level").addEventListener("input", onThresholdChange );
