/*
Regen Open Development 2018
Licensed under the Mozilla Public License Version 2.0.
Refer to README.md and LICENSE.md for details.
*/

/* FALLBACK STRING DISTANCE MODULE:
Determines the visual edit distance between strings using a modification of the
Levenshtein distance algorithm, using a map of visually similar character pairs
and floating point weights.

This is a "pure javascript" fallback that's only used when the rust/wasm can't or
won't load. This module should be keep in sync with the main rust/wasm module as
much as possible, until it is eventually phased out.
*/

class CharPair {
  constructor(chars) {
    this.chars = chars;
  }

  flip() {
    this.chars = this.chars.reverse();
    return this;
  }

  toString() {
    return this.chars.toString();
  }
}

class CharMap {
  constructor() {
    this.items = {};

    // Group 1 (Linears): i I l 1 j t ! |
    this.mapWeightedGroup(['i','I','l','L','1','j','t','T','!','|',], 0.75);
    this.mapWeightedGroup(['i','I','l','1','j','t','!','|'], 0.5);
    this.mapWeightedGroup(['i','l','1','j','|'], 0.25);
    this.mapWeightedGroup(['i','I'], 0.1);

    // Group 2 (Circulars): o O 0 Q e a c 8 6 9
    this.mapWeightedGroup(['o','O','0','Q','e','a','c','b','d','p','P','q','8','6','9'], 0.75);
    this.mapWeightedGroup(['o','O','0','Q','e','a','c'], 0.5);
    this.mapWeightedGroup(['o','O','0','Q'], 0.1);

    // Group 3 (Flat-backs): b d p q k P B R E K
    this.mapWeightedGroup(['b','d','p','q','k','P','B','R','E','K'], 0.75);
    this.mapWeightedGroup(['b','d','p','q','k','g','j','h'], 0.5);
    this.mapWeightedGroup(['P','B','R','E','K'], 0.5);
    this.mapWeightedGroup(['p','P'], 0.1);
    this.mapWeightedGroup(['k','K'], 0.1);

    // Group 4 (Valleys): u U v V w W
    this.mapWeightedGroup(['u','U','v','V','w','W'], 0.5);
    this.mapWeightedGroup(['u','U'], 0.1);
    this.mapWeightedGroup(['v','V'], 0.1);
    this.mapWeightedGroup(['w','W'], 0.1);

    // Group 5 (Hills): n m r h
    this.mapWeightedGroup(['n','m','r','h'], 0.5);

    // Others: E 3, A 4, x X,
    this.mapWeightedGroup(['F','f','E'], 0.75);
    this.mapWeightedGroup(['E','3'], 0.5);
    this.mapWeightedGroup(['A','4'], 0.5);
    this.mapWeightedGroup(['x','X'], 0.1);
  }

  getWeight(chars) {
    let pair = new CharPair(chars);
    if (pair.toString() in this.items) {
      return this.items[pair.toString()];
    } else {
      return 1.0;
    }
  }

  setWeight(chars, weight) {
    let charPair = new CharPair(chars);
    this.items[charPair.toString()] = weight;
    this.items[charPair.flip().toString()] = weight;
  }

  mapWeightedGroup(characters, weight) {
    for (let index=0; index < characters.length; index++){
      for (let neighbor_index=index+1; neighbor_index < characters.length; neighbor_index++){
        this.setWeight([characters[index], characters[neighbor_index]], weight);
      }
    }
  }
}

var charmap = new CharMap();
function newMatrix(cols, rows) {
  let matrix = [];
  for (let ic = 0; ic < cols; ic++) {
    matrix[ic] = [];
    for (let ir = 0; ir < rows; ir++) {
      matrix[ic][ir] = 0.0;
    }
  }
  return matrix;
}

var fallback = {};
fallback.getDifference = function(source_string, target_string) {
  let target_length = target_string.length;
  let source_length = source_string.length;
  let difference;

  if (source_length == 0) {
    difference = target_length;
  } else if (target_length == 0) {
    difference = source_length;
  } else {
    let matrix = newMatrix(source_length + 1, target_length + 1);

    for (let ir = 0; ir < matrix[0].length; ir++) {
      matrix[0][ir] = ir;
    }
    for (let ic = 0; ic < matrix.length; ic++) {
      matrix[ic][0] = ic;
    }

    for (let source_index = 1; source_index < source_length + 1; source_index++) {
      for (let target_index = 1; target_index < target_length + 1; target_index++) {
        let source_char = source_string.charAt(source_index - 1);
        let target_char = target_string.charAt(target_index - 1);

        if (source_char === target_char) { //Maybe bug?
          matrix[source_index][target_index] = matrix[source_index - 1][target_index - 1];
        } else {
          let min = Math.min(
            matrix[source_index - 1][target_index    ],
            matrix[source_index - 1][target_index - 1],
            matrix[source_index    ][target_index - 1]
          );

          let weight = charmap.getWeight([source_char, target_char]);
          matrix[source_index][target_index] = min + weight;
        }
      }
    }

    difference = matrix[source_length][target_length];
  }

  return difference / Math.max(source_length, target_length);
};
