/*
Regen Open Development 2018
Licensed under the Mozilla Public License Version 2.0.
Refer to README.md and LICENSE.md for details.
*/

/*
Main WebExtension background script.
*/

var getDifference = function(source, target){return 0.0;};

wasm_bindgen('./generated/stringdist_bg.wasm').then(() => {
  console.log("WASM load successful.");
  getDifference = wasm_bindgen.get_difference;
  common.webNavigation.onBeforeNavigate.addListener(tryNavigate);
}, (err) => {
  console.log("Couldn't load wasm, err[" + err + "] using javascript fallback instead.");
  getDifference = fallback.getDifference;
  common.webNavigation.onBeforeNavigate.addListener(tryNavigate);
});

function tryNavigate(details) {
  Promise.all([ common.storage.local.get({protectedDomains : []}),
                common.storage.local.get("threshold"),
                common.storage.local.get({whitelistedDomains: []}) ])
  .then( (options) => {
    let threshold = options[1].threshold;
    let protectedDomains = new Set(options[0].protectedDomains);
    let whitelistedDomains = new Set(options[2].whitelistedDomains);

    function tryBroadMatchAttempt(){
      let navigationTarget = extractRootDomain(details.url);
      return (protectedDomains.has(navigationTarget)
              || whitelistedDomains.has(navigationTarget));
    }

    function calculateDifference(protectedDomain){
      let partCount = getDomainPartLength(protectedDomain);
      let navigationTarget = extractDomainByPartCount(details.url, partCount);
      let diff = getDifference(navigationTarget, protectedDomain);

      if( diff !== 0.0 && diff < threshold ) {
        let standardBlockPage = "block.html?difference=" + diff + "&blockedDomain=" + navigationTarget + "&protectedDomain=" + protectedDomain + "&attemptedConnection=" + details.url;
        let mobileBlockPage = "m" + standardBlockPage;

        common.isMobile().then( () => {
          common.tabs.update(details.tabId,{
              loadReplace: true,
              url:mobileBlockPage
            });
        }, (e) => {} );

        common.isDesktop().then( () => {
          console.log("Page Blocked!");
          common.tabs.update(details.tabId,{
              loadReplace: true,
              url:standardBlockPage
            });
        }, (e) => {} );
      }
    }

    if( !tryBroadMatchAttempt() ){
      protectedDomains.forEach(calculateDifference);
    }
  });
}

function extractHostname(url) {
  let hostname;
  if (url.indexOf("//") > -1) {
    hostname = url.split('/')[2];
  } else {
    hostname = url.split('/')[0];
  }
  hostname = hostname.split(':')[0];
  hostname = hostname.split('?')[0];
  return hostname;
}

function getDomainPartLength(domain) {
  return domain.split('.').length;
}

function extractDomainByPartCount(url, parts) {
  let domain = extractHostname(url);
  let splitDomain = domain.split('.');
  splitDomain = splitDomain.slice(Math.max(splitDomain.length - parts, 0), splitDomain.length);

  return splitDomain.join('.');
}

function extractRootDomain(url) {
  let domain = extractHostname(url);
  let splits = domain.split('.');
  let num_splits = splits.length;

  if (splits.length > 2) {
    domain = splits[num_splits - 2] + '.' + splits[num_splits - 1];
    if(splits[num_splits - 2].length == 2 && splits[num_splits - 1].length == 2){
      domain = splits[num_splits - 3] + '.' + domain;
    }
  }
  return domain;
}
