/*
Regen Open Development 2018
Licensed under the Mozilla Public License Version 2.0.
Refer to README.md and LICENSE.md for details.
*/

use std::collections::HashMap;

/*
The charmap maps pairs of visually similar characters to weight values 0~1.
The more visually similar, the lower the weight value.
*/
pub struct CharMap {
    edge_map: HashMap<CharPair, f64>,
}

impl CharMap {
    pub fn new() -> CharMap {
        CharMap{
            edge_map: HashMap::new(),
        }
    }

    pub fn get_weight(&self, pair: (char, char) ) -> Option<&f64> {
        self.edge_map.get(&CharPair::from(pair))
    }

    pub fn set_weight(&mut self, pair: (char, char), weight: f64) {
        let pair = CharPair::from(pair);
        self.edge_map.insert(pair, weight);
        self.edge_map.insert(pair.flip(), weight);
    }

    pub fn map_weighted_group(&mut self, characters: Vec<char>, weight: f64) {
        for (index, character) in characters.iter().enumerate() {
            for neighbor_index in index + 1..characters.len() {
                self.set_weight((*character, characters[neighbor_index]), weight);
            }
        }
    }
}

#[derive(Copy, Clone, PartialEq, Eq, Hash)]
pub struct CharPair( char, char );

impl From<(char,char)> for CharPair {
    fn from(char_tuple: (char,char)) -> Self {
        CharPair(char_tuple.0, char_tuple.1)
    }
}

impl CharPair {
    fn flip(self) -> CharPair {
        CharPair(self.1, self.0)
    }
}
