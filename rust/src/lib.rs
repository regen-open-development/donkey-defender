/*
Regen Open Development 2018
Licensed under the Mozilla Public License Version 2.0.
Refer to README.md and LICENSE.md for details.
*/
#[macro_use]
extern crate lazy_static;
extern crate wasm_bindgen;

mod charmap;
mod stringdist;

use wasm_bindgen::prelude::*;

#[wasm_bindgen]
pub fn get_difference(source: &str, target: &str) -> f64{
    stringdist::get_difference(source, target)
}
