/*
Regen Open Development 2018
Licensed under the Mozilla Public License Version 2.0.
Refer to README.md and LICENSE.md for details.
*/

/* MAIN STRING DISTANCE MODULE:
Determines the visual edit distance between strings using a modification of the Levenshtein distance
algorithm, using a map of visually similar character pairs and floating point weights.
*/

use std::cmp;
use charmap::CharMap;

lazy_static! {
    static ref CHARMAP: CharMap = {
        let mut charmap = CharMap::new();

        /* Map Visually Similar Characters */

        // Group 1 (Linears): i I l 1 j t ! |
        charmap.map_weighted_group(vec!['i','I','l','L','1','j','t','T','!','|',], 0.75);
        charmap.map_weighted_group(vec!['i','I','l','1','j','t','!','|'], 0.5);
        charmap.map_weighted_group(vec!['i','l','1','j','|'], 0.25);
        charmap.map_weighted_group(vec!['i','I'], 0.1);

        // Group 2 (Circulars): o O 0 Q e a c 8 6 9
        charmap.map_weighted_group(vec!['o','O','0','Q','e','a','c','b','d','p','P','q','8','6','9'], 0.75);
        charmap.map_weighted_group(vec!['o','O','0','Q','e','a','c'], 0.5);
        charmap.map_weighted_group(vec!['o','O','0','Q'], 0.1);

        // Group 3 (Flat-backs): b d p q k P B R E K
        charmap.map_weighted_group(vec!['b','d','p','q','k','P','B','R','E','K'], 0.75);
        charmap.map_weighted_group(vec!['b','d','p','q','k','g','j','h'], 0.5);
        charmap.map_weighted_group(vec!['P','B','R','E','K'], 0.5);
        charmap.map_weighted_group(vec!['p','P'], 0.1);
        charmap.map_weighted_group(vec!['k','K'], 0.1);

        // Group 4 (Valleys): u U v V w W
        charmap.map_weighted_group(vec!['u','U','v','V','w','W'], 0.5);
        charmap.map_weighted_group(vec!['u','U'], 0.1);
        charmap.map_weighted_group(vec!['v','V'], 0.1);
        charmap.map_weighted_group(vec!['w','W'], 0.1);

        // Group 5 (Hills): n m r h
        charmap.map_weighted_group(vec!['n','m','r','h'], 0.5);

        // Others: E 3, A 4, x X,
        charmap.map_weighted_group(vec!['F','f','E'], 0.75);
        charmap.map_weighted_group(vec!['E','3'], 0.5);
        charmap.map_weighted_group(vec!['A','4'], 0.5);
        charmap.map_weighted_group(vec!['x','X'], 0.1);

        charmap
    };
}

pub fn get_difference(source: &str, target: &str) -> f64 {
    let source_length = source.chars().count();
    let target_length = target.chars().count();
    let difference;

    if source_length == 0 {
        difference = target_length as f64;
    } else if target_length == 0 {
        difference = source_length as f64;
    } else {
        // Usage: difference_matrix[source_index][target_index]
        let mut difference_matrix: Vec<Vec<f64>> =  vec![vec![0.0;target_length + 1];source_length + 1];

        for (index, value) in difference_matrix[0].iter_mut().enumerate() {
            *value = index as f64;
        }
        for (index, table) in difference_matrix.iter_mut().enumerate() {
            table[0] = index as f64;
        }

        for source_index in 1..source_length + 1 {
            for target_index in 1..target_length + 1 {
                let source_char = source.chars().nth(source_index - 1).unwrap();
                let target_char = target.chars().nth(target_index - 1).unwrap();

                if source_char == target_char {
                    difference_matrix[source_index][target_index] = difference_matrix[source_index - 1][target_index - 1];
                } else {
                    let min = min(difference_matrix[source_index - 1][target_index    ],
                                  difference_matrix[source_index - 1][target_index - 1],
                                  difference_matrix[source_index    ][target_index - 1]);

                    let weight = CHARMAP.get_weight((source_char, target_char)).unwrap_or(&1.0);
                    difference_matrix[source_index][target_index] = min + weight;
                }
            }
        }

        difference = difference_matrix[source_length][target_length];
    }

    difference / cmp::max(source_length, target_length) as f64
}

fn min(v1: f64, v2: f64, v3: f64) -> f64 {
    v1.min(v2.min(v3))
}

#[cfg(test)]
mod tests {
    use super::*;

    fn diff_with_print( s1: &str, s2: &str) -> f64 {
        let diff = get_difference(s1, s2);
        println!("'{}' compared to '{}', difference is {}", s1, s2, diff);
        diff
    }

    #[test]
    fn test_with_urls() {
        //Format: [(ProtectedSite, [Exptected Too Close], [Expected OK])]
        let tests = [ ("gmail.com", ["gmeil.com", "qmail.com", "gmail.cem", "gmail.edu"], ["reddit.com", "ebay.com"]),
                      ("twitter.com", ["twittcr.com", "twiter.com", "twittr.com", "twitter.org"], ["twitch.tv", "paypal.com"]),
                      ("bbc.co.uk", ["bbe.co.uk", "ddc.co.uk", "bbc.eo.uk", "blc.co.uk"], ["independent.co.uk", "amazon.co.uk"]),
                      ("abcdef", ["abcdcf","abedef","abcbef","abc.def"], ["123456", "ABCDEF"]) ];
        let threshold = 0.3;

        for test in tests.iter() {
            assert_eq!(diff_with_print(test.0, test.0), 0.0);
            for positive in test.1.iter() {
                assert!(diff_with_print(test.0, positive) < threshold);
            }
            for negative in test.2.iter() {
                assert!(diff_with_print(test.0, negative) > threshold);
            }
        }
    }
}
