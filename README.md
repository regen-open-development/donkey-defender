# Donkey Defender
A cross-browser extension designed to resist phishing attacks by checking for suspicious and deceptive URLs each time you navigate.

## How to use:
* Install the **Donkey Defender browser extension** for [Mozilla Firefox](https://addons.mozilla.org/en-US/firefox/addon/donkey-defender/) or [Google Chrome](https://chrome.google.com/webstore/detail/donkey-defender/bojniphfjbjdlajlanmaamjhcecdpoan?hl=en).
* Configure your **protected domains list** and **security level**.
* **Browse normally**, fine-tuning your preferences over time if needed.

### Tips:
* We recommend protecting the domains that *really* need to be protected: personal and business websites, mail services, financial sites, private repository hosts, and anything that *holds your personal information or data.*
* The more strict your settings, the more likely you are to encounter *false positives* -- where a website that you want to visit is inadvertently blocked. If this happens you can manually add a site to your **whitelist**, which prevents it from being blocked without needlessly expanding your protected domains list. Adding whitelisted domains allows you to use relatively strict settings without too much interruption to your daily browsing habits.
* Some complex and modern websites or services will connect to multiple different domains in a single navigation, which (if the domains are similar enough) can cause one or more false positive block. This is somewhat uncommon, but it's worth noting. You can work around this by simply adding each of these domains to your whitelist (after checking to make sure that the connections are legitimate and trustworthy, of course!).

## How to contribute:
There are a few meaningful ways to help out if you want to:

* One way to help is by making a *tip or contribution to Regen Open Development* which will allow us to dedicate *more than just our free time to developing free and open source software like this.* Write us an email at studioregen@gmail.com for details.
* Also useful is *submitting feedback, bug reports, and features requests* through this repository.
* Or you can help development directly, by *submitting code, localization, or documentation patches* through this repository. See the "build instructions" below, to get started!

### Build instructions:

*Note:* These instructions are for technical users and developers who want to build their own copy of Donkey Defender.
If you simply want to use the extension, you can install it with a single click through
your browser's add-ons page: [Mozilla Firefox](https://addons.mozilla.org/en-US/firefox/addon/donkey-defender/) / [Google Chrome](https://chrome.google.com/webstore/detail/donkey-defender/bojniphfjbjdlajlanmaamjhcecdpoan?hl=en).

#### First time setup:
* Install the Rust programming language. (https://www.rust-lang.org/en-US/install.html) Installing Rust may require a system reboot.
* Install the Rust WebAssembly build target. (**command:** `rustup target add wasm32-unknown-unknown`)
* Install the Rust WebAssembly JavaScript bindings generator. (**command:** `cargo install wasm-bindgen-cli`)
* Git clone Donkey Defender git repository to your local machine.

#### Each build:
* Git pull latest changes from the repository. `git pull`
* Inside this project's "rust/" directory, build the Rust WebAssembly library. (**command:** `cargo build --target wasm32-unknown-unknown`) Continue by generating the Javascript bindings. (**command:** `wasm-bindgen --no-modules --out-dir ../webextension/generated target/wasm32-unknown-unknown/debug/stringdist.wasm`).
* Load extension using your browser's extension debugging tools. (i.e.: https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/Debugging). This usually requires you to direct your browser to `webextension/manifest.json` and will assign it a temporary plugin ID.

#### Android Testing and Debugging:
* Install Android Studio SDK and make sure adb is installed.
* Install NodeJS.
* Install NPM utility.
* Run `npm install --global web-ext`
* Run `web-ext run --target=android-firefox`. This will likely not work, but will quickly return the device id. You can now run `web-ext run --target=firefox-android --android-device=<android-device-id>` with the newly obtained id. Alternatively, you can get the id by running `adb devices`.

#### Building to Zip File.
* When building for deployment, you will need web-ext (check android testing instructions above) in order to compile.
* Navigate to the root of this project and run `web-ext build --source-dir ./webextension/ --artifacts-dir ./releases`.

## License:
This project and all of its contents are being released under the Mozilla Public License 2.0. For complete license details, please read "LICENSE.md"!
